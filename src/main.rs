use crate::config::load_config;
use crate::simulation::Simulation;

mod config;
mod simulation;

fn main() {

    // Load configuration
    let cfg = load_config();

    // Construct Simulation
    let simulation = Simulation::new(cfg);

    simulation.run();
}
