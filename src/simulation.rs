use std::time::Duration;
use std::thread::sleep;
use rand::Rng;
use crate::config::Config;
use physics_2d::object::particle::Particle;
use physics_2d::geometry::vector::Vector2D;

pub struct Simulation {
    acceleration_gravity:   f64,
    simulation_duration:    f64,
    time_step:              f64,
    objects:                Vec<Particle>,
}

impl Simulation {
    pub fn new(cfg: Config) -> Simulation {
        Simulation{
            acceleration_gravity:   cfg.engine.acceleration_gravity,
            simulation_duration:    cfg.simulation.simulation_duration,
            time_step:              cfg.simulation.time_step,
            objects:                init_particles(cfg.simulation.number_of_objects),
        }
    }

    pub fn run(mut self) {
        let mut current_time = 0.0;
        let dt = self.time_step;

        while current_time < self.simulation_duration {
            // Todo: Impl this correctly (time = GetTime(), dt = current - previous, previous = current)
            sleep(Duration::from_secs_f64(dt));

            // Loop through all particles
            for mut particle in &mut *self.objects {

                // Calculate particle force & acceleration
                let force = particle.compute_force_gravity(self.acceleration_gravity);

                // Todo: Move below calculations to the engine
                let acceleration = Vector2D{
                    x: force.x / particle.mass,
                    y: force.y / particle.mass,
                };

                // Update particle 2D velocity
                particle.velocity.x += acceleration.x * dt;
                particle.velocity.y += acceleration.y * dt;

                // Update particle 2D position
                particle.position.x += particle.velocity.x * dt;
                particle.position.y += particle.velocity.y * dt;

                // Mass has not changed
            }
            // Display visuals
            self.display_particles();

            // Increment current_time
            current_time += dt;
        }
    }

    fn display_particles(&self) {
        for (index, particle) in self.objects.iter().enumerate() {
            println!("Particle[{}]: ({}, {})", index, particle.position.x, particle.position.y)
        }
    }
}

fn init_particles(number_of_particles: u128) -> Vec<Particle> {
    let mut particles: Vec<Particle> = vec![];

    // Create number_of_particles using random Particle attributes
    for _ in 0..number_of_particles {
        particles.push(Particle{
            position:   Vector2D::new_random(),
            velocity:   Vector2D::new_random(),
            mass:       rand::thread_rng().gen(),
        });
    }
    return particles
}